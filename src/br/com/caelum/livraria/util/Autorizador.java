package br.com.caelum.livraria.util;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import br.com.caelum.livraria.modelo.Usuario;

public class Autorizador implements PhaseListener {

	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(PhaseEvent evento) {
		
		//Pega Contexto.
		FacesContext context = evento.getFacesContext();
		
		//Retorna a página
		String nomePagina = evento.getFacesContext().getViewRoot().getViewId();
		System.out.println(nomePagina);
		
		
		// Se a página for igual /login.xhtml não interceptar pois não podemos travar a página login
		if(nomePagina.equals("/login.xhtml"))return;
		
		Usuario usuarioLogado = (Usuario) context.getExternalContext().getSessionMap().get("usuarioLogado"); 
		
		
		// Se o usuario logado é direfente de nulo, ou seja ele está logado então retornar a página solicitada
		if(usuarioLogado != null){ 
			return;
		}
		
		
		// Caso tente navegar e não está logado redirecionara para página Login
		NavigationHandler handler = context.getApplication().getNavigationHandler();
		handler.handleNavigation(context, null, "/login?faces-redirect=true");
		context.renderResponse();
		
		
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		System.out.println("FASE: " + event.getPhaseId());
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
