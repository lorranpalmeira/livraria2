package br.com.caelum.livraria.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.caelum.livraria.dao.UsuarioDao;
import br.com.caelum.livraria.modelo.Usuario;

@Named
@ViewScoped
public class LoginBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Usuario usuario = new Usuario();
	UsuarioDao user = new UsuarioDao();
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void logout(){
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		context.getExternalContext().getSessionMap().put("usuarioLogado", null);
		
	}
	
	
	

	public String efetuarLogin(){
		System.out.print("Logou com email " + this.usuario.getEmail());
		System.out.println(" e senha " + this.usuario.getSenha());
		
		// Contexto da Instancia
		FacesContext context = FacesContext.getCurrentInstance();
		
		if(user.existe(this.usuario.getEmail(), this.usuario.getSenha() ) == true ){
			System.out.println("Sucesso no login será redirecionado a página livro.xhtml");
			
			// Se o usuário existe então coloca a chave em "usuarioLogado" que será usada pelo autorizador
			context.getExternalContext().getSessionMap().put("usuarioLogado", this.usuario );
			
			return "livro?faces-redirect=true";
		}//else{
			System.out.println("Usuário não existe no banco favor cadastrar");
			System.out.println("insert into usuario(email,senha) values('seu email aqui','sua senha aqui'); ");
			
			context.getExternalContext().getFlash().setKeepMessages(true); // Mantem o escopo por duas mensagens
			context.addMessage(null, new FacesMessage("Usuário não encontrado!"));
			
			return "login?faces-redirect=true";
			
		//}
		

		
	}

}
